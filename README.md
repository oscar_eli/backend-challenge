### O desafio

O objetivo desse desafio é apresentar seus conhecimentos em (Python3, NodeJS ou .Net Core) sobre APIs RestFULL, operações com bancos de dados, uso de arquivo de configuração, testes automatizados com cobertura, logging, controle de versões, documentação de código e de uso e estrutura/qualidade de código.

A aplicação a ser desenvolvida será o backend para gerenciar listas de tarefas.

Ela deve ser constuída em (Python, NodeJS ou .Net Core) e você pode utilizar um framework e o banco de dados de sua preferência.

A API deve conter os seguintes endpoints:

- **/taskList**: retorna todas as lista cadastradas e permite criar uma nova lista. Cada lista possui zero ou mais tarefas.
- **/taskList/id**: permite a edição, alteração e remoção de uma lista específica
- **/tasks**: retorna todas as tarefas de uma lista e permite criar uma nova tarefa. Cada tarefa esta sempre associada à uma lista.
- **/tasks/id**: permite a edição, alteração e remoção de uma tarefa
- **/tags**: retorna todas as tags cadastradas. Cada tag pode estar associada à uma ou mais tarefas.
- **/tags/id**: permite a edição, alteração e remoção de uma tag.

Para assegurar a correta comunicação com um hipotético aplicativo em frontend que gerenciará as tasks, o seguinte contrato de API deve ser seguido para cada model:

- TaskLists

  - Id: uuid
  - Name: string

- Tags

  - Id: uuid
  - Name: string
  - Count: int (O número de tasks utilizando a tag)

- Tasks
  - Id: uuid
  - Title: string
  - Notes: string
  - Priority: integer
  - RemindMeOn: date
  - ActivityType: string (indoors, outdoors)
  - Status: string (open, done)
  - TaskList: uuid
  - Tags: list

### O que esperamos de você:

- Utilize os verbos HTTP (GET, POST, PUT, PATCH, DELETE) corretamente
- Retorne estados HTTP coerentes (200, 404 etc)
- Escreva testes e apresente o relatório de cobertura dos mesmos, afinal precisamos garantir o funcionamento e a qualidade :)
- Escreva documentação do código, suas funções e assinaturas
- Crie logs com classificações (INFO, WARN, ERROR, DEBUG) coerentes
- Utilize virtualização e ferramenta de controle de dependências
- Pense no histórico de remoções (Solução para manter dados históricos deletados; Dados deletados deverão ser preservados)
- Aderência aos padrões de qualidade de código vigentes na comunidade

### Diferenciais:

- Aplicação rodando em ambiente Docker

### Entrega:

- Faça um fork deste repositório para sua conta pessoal no bitbucket, se certifique de que esteja público, e quando finalizar, responda no e-mail do desafio com o link do seu repositório.

## Commiting

A commit message can consists of a **header**, **body** and **footer**. The header is the only mandatory part and consists of a type and a subject. The body is used to fully describe the change. The footer is the place to reference any issues or pull requests related to the commit. That said, we end with a template like this:

```
<type>: <subject>

[optional body]

[optional footer]
```

To ensure that a commit is valid, easy to read, and changelog-ready, we have a hook that lints the commit message before allowing a commit to pass. This linter verifies the following:

- The header (first line) is the only mandatory part of the commit message;
- The body and footer are both optional but its use is highly encouraged;
- The header should contains:
  - A type:
    - Must be lowercase;
    - Must be one of:
      - **chore**: A change that neither fix a bug nor adds a feature;
      - **ci**: A CI change;
      - **docs**: A documentation change or fix;
      - **feat**: A new feature;
      - **fix**: A bug fix;
      - **test**: A test-related change.
  - A subject:
    - Must be capitalized;
    - Must be limited to 50 characters or less;
    - Must omit any trailing punctuation.
- The body:
  - Must have a leading blank line;
  - Each line must be limited to 72 characters or less.
- The footer:
  - Must have a leading blank line;
  - Each line must be limited to 72 characters or less;
  - If your commit is about documentation or meta files, please add the tag **[skip ci]** to skip the building process.
  - If needed, reference to issues and pull requests must be made here in the last line.

You also should follow these general guidelines when committing:

- Use the present tense ("Add feature" not "Added feature");
- Use the imperative mood ("Move cursor to..." not "Moves cursor to...");
- Try to answer the following questions:
  - Why is this change necessary?
  - How does it address the issue?
  - What side effects (if any) does this change may have?

Example of a commit message:

```
type: Commit message style guide for Git

The first line of a commit message serves as a summary.  When displayed
on the web, it's often styled as a heading, and in emails, it's
typically used as the subject. As such, you should specify a "type" and
a "subject". The type must be lowercase and one of: chore, ci, docs,
feat, fix, test. For the subject you'll need capitalize it and
omit any trailing punctuation. Aim for about 50 characters, give or
take, otherwise it may be painfully truncated in some contexts. Write
it, along with the rest of your message, in the present tense and
imperative mood: "Fix bug" and not "Fixed bug" or "Fixes bug".
Consistent wording makes it easier to mentally process a list of
commits.

Oftentimes a subject by itself is sufficient. When it's not, add a
blank line (this is important) followed by one or more paragraphs hard
wrapped to 72 characters. Git is strongly opinionated that the author
is responsible for line breaks; if you omit them, command line tooling
will show it as one extremely long unwrapped line. Fortunately, most
text editors are capable of automating this.

Issues and pull request can be referenced on the footer: #3 #12
```
